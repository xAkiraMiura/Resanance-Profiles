module.exports = (core) => {
    const fs = require('fs');
    var memory = require('./data/last.json');
    var inbound = process.env.appdata + '\\Resanance\\sounds.txt';
    var inboundparity = process.env.appdata + '\\Resanance\\storage\\' + memory.last + '.txt';
    var outbound = process.env.appdata + '\\Resanance\\storage\\' + core + '.txt';
    var outboundparity = process.env.appdata + '\\Resanance\\sounds.txt';
    memory.last = core;
    var writeObject = JSON.stringify(memory);
    fs.writeFileSync('./data/last.json', writeObject, (err) => {
        if (err) core.log('Error! ' + err);
    });
    fs.renameSync(inbound, inboundparity, function (err) {
        if (err) core.log('Error! ' + err);
    });
    fs.renameSync(outbound, outboundparity, function (err) {
        if (err) core.log('Error! ' + err);
    });
};