module.exports.log = (msg) => {
    log(msg, 0);
};
module.exports.warn = (msg) => {
    log(msg, 1);
};
module.exports.error = (msg) => {
    log(msg, 2);
};

const log = (msg, sev) => {
    
    var date = new Date();
    var callbk = format(date);
    var string = callbk[0];
    switch (sev) {
    case 0:
        string = string + '[INFO] ';
        break;
    case 1:
        string = string + '[WARN] ';
        break;
    case 2:
        string = string + '[FAIL] ';
        break;
    }
    string = string + `(${msg})`;
    console.log(string); /* eslint-disable-line no-console */
    /* I use VSCode so I MUST use console.log to see stuff*/
};
const format = (date) => {
    var x = ['[', 'false'];
    if (date === undefined) {
        date = new Date();
        x[1] = true;
    }
    var i = 0;
    while (i < 5) {
        if (i !== 4) {
            switch (i) {
            case (0):
                time = date.getHours();
                break;
            case (1):
                time = date.getMinutes();
                break;
            case (2):
                time = date.getSeconds();
                break;
            case (3):
                time = date.getMilliseconds();
                break;
            }
            if (i !== 3) {
                if (time < 10) {
                    var time = '0' + time; // Add digit to create 2 digits
                }
            } else {
                switch (time.toString().length) { // Add digit(s) to make it 3 digits
                case 0:
                    time = time + '000';
                    break;
                case 1:
                    time = time + '00';
                    break;
                case 2:
                    time = time + '0';
                    break;
                case 3:
                    break;

                }
            }
            if (i !== 3) {
                x[0] = x[0] + time + ':'; // Add to array
            } else {
                x[0] = x[0] + time; // Add to last item to array
            }
        } else {
            x[0] = x[0] + '] '; // Package off the standardized time
        }
        i++; // Move forward in loop
    }
    return x; // Send off package
};
