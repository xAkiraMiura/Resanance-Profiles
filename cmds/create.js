const fs = require('fs');
const obj = require('../data/list.json');
const template = require('../template/newSoundkit.json');
module.exports = (core) => {
    obj.profiles.push(core.args[0]);
    var writeObj = JSON.stringify(obj);
    var def = JSON.stringify(template.data);
    def = def.replace(/"/g, '');
    fs.writeFileSync('./data/list.json', writeObj, (err) => {
        if (err) core.log('Error! ' + err);
    });
    fs.writeFileSync(process.env.appdata + '\\Resanance\\storage\\'+ core.args[0] + '.txt', def, (err) => {
        if (err) core.log('Error! ' + err);
    });
};