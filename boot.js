const discriminator = require('./descriminator.js');
const core = require('./compile.js');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});
core.log('Which soundkit do you want to load?');
rl.question('> ', (reply) => {
    core.reply = reply;
    discriminator(core);
});