const stager = require('./stager.js');
module.exports = (core) => {
    var x = core.reply.toLowerCase();
    var temp1 = 0;
    var temp2 = 0;
    if (x.charAt(core.config.commandPrefix.length - 1) === core.config.commandPrefix) {
        var x2 = x.trim().substr(core.config.commandPrefix.length);     /* This code is taken and modified from my other */
        core.command = x2.substr(0, x2.indexOf(' ')).toLowerCase();                /* repo at gitlab.com/SitsuBot/Shard/ */
        if (core.command === '') { // If 0 args.
            core.command = x2.toLowerCase();
        }
        core.args = x2.slice(core.command.length).trim().split(/ +/g);
        while (temp2 < core.commands.all.length) {
            if (temp2 < 999999) {
                if (core.command == core.commands.all[temp2]) { /* Switch-Case is not modular */
                    eval('core.cmds.' + core.command + '(core);'); /* It's injectable but you would only hurt yourself so I don't care. */
                    temp2 = 999999;
                }
            }
            temp2++;
        }
        if (temp2 < 999999) {
            core.warn('Unknown Command.');
        }
    } else {
        while (temp1 < core.list.profiles.length) {
            if (temp1 < 999999) {
                if (x === core.list.profiles[temp1]) {
                    stager(x);
                    temp1 = 999999;
                }
            }
            temp1++;
        }
        if (temp1 < 999999) {
            core.warn('No profile found. use '+ 
core.config.commandPrefix +'create to make a new profile or use ' + 
core.config.commandPrefix + 'help to see other commands');
        }
    }
    process.exit();
};