
module.exports.discriminator = require('./descriminator.js');
const logger = require('./log.js');
module.exports.log = logger.log;
module.exports.warn = logger.warn;
module.exports.error = logger.error;
const readline = require('readline');
module.exports.rl = readline.createInterface({input: process.stdin, output: process.stdout});
module.exports.list = require('./data/list.json');
module.exports.last = require('./data/last.json');
module.exports.config = require('./data/config.json');
module.exports.commands = require('./data/commands.json')
module.exports.cmds = require('./cmds/compile.js');